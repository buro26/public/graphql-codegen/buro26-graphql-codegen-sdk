# GraphQL Codegen SDK

Generate a SDK for your GraphQL API using GraphQL Codegen.

## Getting started

```bash
npm i buro26-graphql-codegen-sdk
```

## Usage

Create a codegen.ts file in the root of your project with the following content:

Add a script to your package.json to run the codegen:

```json
{
  "scripts": {
    "codegen": "graphql-codegen"
  }
}
```

Run the codegen script:

```bash
npm run codegen
```

## Configuration

Create a codegen.ts file in the root of your project with the following content:

```typescript
import {createConfig} from 'buro26-graphql-codegen-sdk'


export default createConfig({
    schema: 'http://localhost:1337/graphql',
    documents: 'src/lib/my-api/queries/**/!(*.generated).{ts,tsx}',
    typesImportPath: '@/lib/my-api/generated/types',
    schemaImportPath: '@/lib/my-api/generated/schema',
    types: {
        path: 'src/lib/my-api/generated/types.ts'
    },
    sdl: {
        path: './schema.graphql'
    },
    client: {
        path: 'src/lib/my-api/generated/client-factory.ts',
        config: {
            logger: true
        }
    },
    zod: {
        path: 'src/lib/my-api/generated/schema.ts',
        config: {
            exportFormDataSchema: true,
        },
    }

})
```

## Usage

Add a script to your package.json to run the codegen:

```json
{
  "scripts": {
    "codegen": "graphql-codegen"
  }
}
```

Run the codegen script:

```bash
npm run codegen
```

### Client

On more how to use the generated client, see
the [buro26-graphql-codegen-client](https://gitlab.com/buro26/public/graphql-codegen-client) package.

### Zod Schema

On more how to use the generated zod schema, see
the [buro26-graphql-codegen-zod](https://gitlab.com/buro26/public/graphql-codegen-zod) package.

## Test and Deploy

### Running tests

To run tests, run the following command:

```bash
bun test
```

## Contributing

Wish to contribute to this project? Pull the project from the repository and create a merge request.

## Authors and acknowledgment

Buro26 - https://buro26.digital  
Special thanks to all contributors and the open-source community for their support and contributions.

## License

This project is licensed under the MIT License - see the LICENSE file for details.

## Project status

The project is currently in active development. We are continuously working on adding new features and improving the
existing ones. Check the issues section for the latest updates and planned features.

Feel free to reach out if you have any questions or suggestions!