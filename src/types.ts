import {CodegenConfig} from "@graphql-codegen/cli";
import {Config as ClientConfig} from 'buro26-graphql-codegen-client'
import {Config as SchemaConfig} from 'buro26-graphql-codegen-zod'
import {TypeScriptPluginConfig} from '@graphql-codegen/typescript'
import {TypeScriptDocumentsPluginConfig} from '@graphql-codegen/typescript-operations'
import {TypeScriptTypedDocumentNodesConfig} from '@graphql-codegen/typed-document-node'

type ClientConfigParams = Omit<Omit<ClientConfig, 'typesImportPath'>, 'schemaImportPath'>
type ZodConfigParams = Partial<Omit<SchemaConfig, 'typesImportPath'>>
type TypeScriptConfigParams = TypeScriptPluginConfig & TypeScriptDocumentsPluginConfig & TypeScriptTypedDocumentNodesConfig

export type Config = Partial<Omit<CodegenConfig, 'schema'>> & {
    schema: CodegenConfig['schema']
    typesImportPath: string
    schemaImportPath: string
    sdl: {
        path: string
    },
    types: {
        path: string
        config?: TypeScriptConfigParams
    },
    client: {
        path: string
        config?: ClientConfigParams
    },
    zod: {
        path: string
        config?: ZodConfigParams
    },
}