import {CodegenConfig} from "@graphql-codegen/cli";
import {Config} from "@/types";


export const createConfig = ({sdl, client, zod, types, typesImportPath, schemaImportPath, ...config}: Config) => {
    const toGenerate: CodegenConfig['generates'] = {
        [sdl?.path]: {
            plugins: ['schema-ast'],
        },
        [types.path]: {
            plugins: ['typescript', 'typescript-operations', 'typed-document-node'],
            config: {
                withHooks: false,
                maybeValue: 'T | null',
                avoidOptionals: false,
                ...types.config,
            }
        },
        [client.path]: {
            plugins: ['buro26-graphql-codegen-client'],
            config: {
                logger: true,
                typesImportPath,
                schemaImportPath,
                ...client.config,
            }
        },
        [zod.path]: {
            plugins: ['buro26-graphql-codegen-zod'],
            config: {
                typesImportPath,
                onlyWithValidation: false,
                lazy: true,
                exportFormDataSchema: false,
                ...zod.config,
                zodTypesMap: {
                    DateTime: 'string',
                    JSON: 'string',
                    ID: 'string',
                    Int: 'number',
                    Float: 'number',
                    Long: 'number',
                    String: 'string',
                    Boolean: 'boolean',
                    I18NLocaleCode: 'string',
                    ...zod.config?.zodTypesMap,
                },
                zodSchemasMap: {
                    DateTime: 'z.string()',
                    JSON: 'z.any()',
                    Long: 'z.coerce.number()',
                    I18NLocaleCode: 'z.string()',
                    ...zod.config?.zodSchemasMap,
                }
            }
        }
    }

    return {
        ...config,
        generates: {
            ...toGenerate,
            ...config.generates
        }
    }
}